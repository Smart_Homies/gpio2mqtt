# gpio2mqtt

This is meant to remote control GPIO connected devices via MQTT on a
Raspberry Pi. Currently LEDs and buzzers are supported.

## WARNING

USE AT YOUR OWN RISK. NO WARRANTY AT ALL.

Toggling GPIO pins is potentially harmful, if you do not know what you are
doing.

## Building

Make sure the following packages are installed:
- libinih-dev
- libpaho-mqtt-dev
- libgpiod-dev
- cmake

```
cmake .
make
```

## Running

If you are lazy, just run sudo ./gpio2mqtt

However it is more secure to run this a normal user.
On Raspberry Pi OS you can just add  a user to the "gpio" group.

On Debian you would have to create a new group, and add a udev rule for
/dev/grpiochip*


## Testing

The supplied gpio2mqtt.ini configures GPIO pins for the "fish dish" hat,
connects to the mqtt server running on localhost and listens on the topic
"fishdish/#"

You can turn each LED on and off individually for now and let the buzzer
beep for n seconds

```
mosquitto_pub -t fishdish/led_red -m off
mosquitto_pub -t fishdish/led_yellow -m on
mosquitto_pub -t fishdish/led_green -m off
mosquitto_pub -t fishdish/buzzer -m 1.5  # buzz for 1.5 seconds

```

## Configuration

devices are specified under the GPIO section in gpio2mqtt.ini, the prefix led_
and buzzer_ specify the device type. It is possible to just use "buzzer" and
"led". Duplicated entries will be  accepted. For example if you have  LEDs 
connected to GPIO 4 and 9 which you want to control at the same time you can
simply do this:

```
[GPIO]
led_xyz = 4
led_xyz = 9
```

Now both will react to:

```
mosquitto_pub -t fishdish/led_xyz -m on
```
