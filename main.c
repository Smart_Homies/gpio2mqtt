/*
    Copyright (C) 2022-2023 Andreas Shimokawa

    This file is part of gpio2mqtt

    gpio2mqtt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gpio2mqtt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <MQTTClient.h>
#include <ini.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mqtt.h"
#include <gpiod.h>

#define MAX_DEVICES 16

#define TYPE_UNKNOWN 0
#define TYPE_LED 1
#define TYPE_BUZZER 2

#define HIGH 1
#define LOW 0

typedef struct {
    const char *name;
    int gpio_pin;
    int type;
    struct gpiod_line *line;
} gpio_device_config;

typedef struct {
    const char *MQTT_server;
    const char *MQTT_username;
    const char *MQTT_password;
    const char *MQTT_clientid;
    const char *MQTT_topic;
    const char *GPIO_chip;
    gpio_device_config *gpio_device_configs[MAX_DEVICES];
    int device_count;
    bool gpio_initialized;
} configuration;

configuration config;

static int inihandler(void *user, const char *section, const char *name, const char *value)
{
    configuration *pconfig = (configuration *)user;

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("MQTT", "server")) {
        pconfig->MQTT_server = strdup(value);
    }
    else if (MATCH("MQTT", "username")) {
        pconfig->MQTT_username = strdup(value);
    }
    else if (MATCH("MQTT", "password")) {
        pconfig->MQTT_password = strdup(value);
    }
    else if (MATCH("MQTT", "topic")) {
        pconfig->MQTT_topic = strdup(value);
    }
    else if (MATCH("MQTT", "clientid")) {
        pconfig->MQTT_clientid = strdup(value);
    }
    else if (MATCH("GPIO", "chip")) {
        pconfig->GPIO_chip = strdup(value);
    }
    else if (strcmp(section, "GPIO") == 0) {
        char *token = strdup(name);
        token = strtok(token, "_");
        int device_type = TYPE_UNKNOWN;
        if ((token && strcmp(token, "led") == 0) || (strcmp(name, "led") == 0)) {
            device_type = TYPE_LED;
        }
        else if ((token && strcmp(token, "buzzer") == 0) || (strcmp(name, "buzzer") == 0)) {
            device_type = TYPE_BUZZER;
        }
        else {
            printf("WARNING: unknown type for device name %s\n", name);
            free(token);
            return 0;
        }
        gpio_device_config *devconfig = malloc(sizeof(devconfig));
        devconfig->name = strdup(name);
        devconfig->gpio_pin = atoi(value);
        devconfig->type = device_type;
        pconfig->gpio_device_configs[pconfig->device_count++] = devconfig;
        printf("parsed device #%d (%s) on GPIO pin %d\n", pconfig->device_count, devconfig->name, devconfig->gpio_pin);
        free(token);
    }
    else {
        return 0; /* unknown section/name, error */
    }
    return 1;
}

void set_line(struct gpiod_line *line, int state)
{
    if (line) {
        gpiod_line_set_value(line, state);
    }
}

void set_all_leds(int state)
{
    for (int i = 0; i < config.device_count; i++) {
        if (config.gpio_device_configs[i]->type == TYPE_LED) {
            set_line(config.gpio_device_configs[i]->line, state);
        }
    }
}

int mqtt_message_callback(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    char *payload = (char *)message->payload;

    printf("Message %s arrived on topic %s\n", payload, topicName);

    char *token;
    char *device;
    char *command;
    token = strtok(topicName, "/");
    if (!token) {
        return 1;
    }
    device = strtok(NULL, "/");
    if (!device) {
        return 1;
    }

    int device_type = TYPE_UNKNOWN;

    bool found = false;
    for (int i = 0; i < config.device_count; i++) {
        if (!strcmp(device, config.gpio_device_configs[i]->name)) {
            struct gpiod_line *line = config.gpio_device_configs[i]->line;

            device_type = config.gpio_device_configs[i]->type;
            if (device_type == TYPE_LED) {
                int state = LOW;
                if (!strcmp(payload, "on")) {
                    state = HIGH;
                }
                else if (!strcmp(payload, "off")) {
                    state = LOW;
                }
                else {
                    printf("WARNING: unknown payload %s for device type LED\n", payload);
                    return 1;
                }
                if (config.gpio_initialized) {
                    set_line(line, state);
                }
            }
            else if (device_type == TYPE_BUZZER) {
                float delay = atof(payload);
                printf("will beep for %fs\n", delay);
                if (config.gpio_initialized) {
                    set_line(line, HIGH);
                    usleep(delay * 1000000);
                    set_line(line, LOW);
                }
            }
            else {
                printf("WARNING: unknown device type for device %s\n", device);
            }
            found = true;
        }
    }
    if (!found) {
        printf("WARNING: unknown device %s\n", device);
    }
    return 1;
}

int setup_libgpiod(const char *chipname)
{
    if (!chipname) {
        return -1;
    }

    struct gpiod_chip *chip;
    chip = gpiod_chip_open_by_name(chipname);
    if (!chip) {
        perror("libgpiod: chip_open failed\n");
        return -1;
    }

    for (int i = 0; i < config.device_count; i++) {
        config.gpio_device_configs[i]->line = gpiod_chip_get_line(chip, config.gpio_device_configs[i]->gpio_pin);
        if (!config.gpio_device_configs[i]->line) {
            perror("libgpiod: get_line failed\n");
            continue;
        }
        int ret = gpiod_line_request_output(config.gpio_device_configs[i]->line, "test", 0);
        if (ret < 0) {
            perror("libgqpiod: line_request_output failed\n");
        }
    }
}

int main(int argc, char **argv)
{
    // defaults
    memset(&config, 0, sizeof(config));

    if (ini_parse("gpio2mqtt.ini", inihandler, &config) < 0) {
        perror("Can't load gpio2mqtt.ini - please copy gpio2mqtt.ini.sample to gpio2mqtt.ini and customize");
        return -1;
    }

    int res = setup_libgpiod(config.GPIO_chip);
    if (res < 0) {
        perror("Could not initialize libgppiod");
    }
    else {
        config.gpio_initialized = true;
    }

    if (config.MQTT_server && config.MQTT_topic) {
        int res =
            mqtt_init(config.MQTT_server, config.MQTT_username, config.MQTT_password, config.MQTT_clientid, config.MQTT_topic, mqtt_message_callback);
        if (!res) {
            printf("now listinging for incoming messages on topic %s\n", config.MQTT_topic);
        }
        else {
            perror("could not connect to mqtt server");
            exit(-1);
        }
    }
    else {
        printf("WARNING: MQTT configuration is incomplete, won't connect to mqtt server\n");
    }

    if (config.gpio_initialized) {
        set_all_leds(HIGH);
        sleep(1);
        set_all_leds(LOW);
    }

    while (true) {
        sleep(10);
    }
}
