/*
    Copyright (C) 2022-2023 Andreas Shimokawa

    This file is part of gpio2mqtt

    gpio2mqtt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gpio2mqtt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <MQTTClient.h>
#include <string.h>
#include <unistd.h>

MQTTClient mqtt_client;
MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
const char *topic = NULL;
const int qos = 1;

void cl_callback(void *context, char *cause)
{
    fputs("connection to MQTT broker lost\n", stderr);

    while (1) {
        fputs("trying to re-connect\n", stderr);
        int res = MQTTClient_connect(mqtt_client, &conn_opts);
        if (!res) {
            fputs("success!\n", stderr);
            break;
        }
        else {
            perror("could not re-connect");
            sleep(1);
            continue;
        }
    }

    MQTTClient_subscribe(mqtt_client, topic, qos);
}

int mqtt_init(const char *mqtt_server, const char *mqtt_username, const char *mqtt_password, const char *mqtt_clientid, const char *_topic,
              MQTTClient_messageArrived ma_callback)
{
    if (_topic == 0) {
        return -1;
    }

    topic = _topic;

    MQTTClient_create(&mqtt_client, mqtt_server, mqtt_clientid, MQTTCLIENT_PERSISTENCE_NONE, NULL);

    int res = MQTTClient_setCallbacks(mqtt_client, NULL, cl_callback, ma_callback, NULL);

    conn_opts.keepAliveInterval = 10;
    conn_opts.cleansession = 1;
    conn_opts.username = mqtt_username;
    conn_opts.password = mqtt_password;

    res = MQTTClient_connect(mqtt_client, &conn_opts);
    if (res) {
        return res;
    }
    MQTTClient_subscribe(mqtt_client, topic, qos);

    return 0;
}

int mqtt_deinit()
{
    int timeout = 100;
    MQTTClient_unsubscribe(mqtt_client, topic);
    MQTTClient_disconnect(mqtt_client, timeout);
    MQTTClient_destroy(&mqtt_client);

    return 0;
}
