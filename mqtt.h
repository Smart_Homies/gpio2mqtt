/*
    Copyright (C) 2022 Andreas Shimokawa

    This file is part of gpio2mqtt

    gpio2mqtt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gpio2mqtt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MQTT_H__
#define __MQTT_H__
int mqtt_init(const char *mqtt_server, const char *mqtt_username, const char *mqtt_password, const char *mqtt_clientid, const char *_topic,
              MQTTClient_messageArrived ma_callback);
int mqtt_deinit();
#endif
